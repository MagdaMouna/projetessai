package projet1;

public class TriangePascal {

	public static void main(String[] args) {

		char model;
		String motif = "";

		if (args.length < 2 || args[0] == "-help") {
			System.out.println("Usage: java Triangle -taille <chiffre> -motif '*' par defaut ");

		} else if (!args[0].equals("-taille")) {
			System.out.println("ERREUR: L'argument -taille est obligatoire");
			System.out.println("Usage: java Triangle -taille <chiffre> -motif '*' par defaut");
			return;
		} else {

			try {
				int n = Integer.parseInt(args[1]);
				if (args.length == 2) {
					model = '*';
				} else {
					if (!args[2].equals("-motif")) {
						System.out.println("Usage: java Triangle -taille <chiffre> -motif '*' par defaut");
						return;
					}
					if (args[3].length() > 1) {
						System.out.println("Un seul caract�re pour le motif");
						return;
					}
					model = args[3].charAt(0);
				}

				for (int i = 0; i < n; i++) {
					motif += model;
					System.out.println(motif);
				}
			} catch (Exception e) {
				System.out.println("ERREUR: la taille doit �tre un entier");
			}

		}

	}

}
